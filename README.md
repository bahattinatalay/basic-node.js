# Sample Node.js Product Management API

This is a sample Node.js application demonstrating basic CRUD (Create, Read, Update, Delete) operations on a product catalog. It's designed for educational purposes and uses a simple in-memory array (`data.js`) for data storage instead of a real database connection.

## Getting Started

### Prerequisites

1. A Node.js, Express.js app placed in a gitlab repository,
2. An EC2 instance with port 22 enabled for inbound traffic, (I have used Ubuntu Image)
3. 80, 443, 5000 ports will be needed.
4. SSH key that you launched your ec2 instance with,
5. Node.js, npm, and pm2 installed on the instance,
6. GitLab Maintainer access

<!-- Nginx installed, configured on the instance, -->

### Installation

1. Clone the repository to your local machine.
2. Navigate to the project directory and run `npm install` to install dependencies.

You should set up the following environment variables in GitLab:

EC2_HOST: The hostname or IP address of your EC2 instance.

EC2_USERNAME: The username that you use to log into your EC2 instance.

EC2_PRIVATE_KEY: The private key that you use to authenticate with your EC2 instance.

### Running the Application

Execute `node app.js` to start the server. It will listen on port 5000.

## Features

- **List all products**: `GET /api/products`
- **Get a single product by ID**: `GET /api/products/:productID`
- **Add a new product**: `POST /api/products`
- **Update an existing product**: `PUT /api/products/:productID`
- **Delete a product**: `DELETE /api/products/:productID`

## Data Structure

The `data.js` file contains an array of product objects. Each product has an `id`, `name`, and `price`.

Example:
```javascript
[
    { id: 1, name: 'iPhone', price: 800 },
    { id: 2, name: 'iPad', price: 650 },
    { id: 3, name: 'iWatch', price: 750 }
]
```

## Note

- This application is for demonstration purposes only and does not persist data between server restarts.
- The data management is done through a static JavaScript array (`data.js`) instead of a real database.

## Contributing

Feel free to fork this repository and submit pull requests for any improvements or fixes.
